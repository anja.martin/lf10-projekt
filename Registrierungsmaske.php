<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="Index.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="http://localhost/Epoquesite/css/index.css"> 

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js">
    </script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js">
    </script>
</head>

<body>
    
<div>
    <form action="post" class="align-right text-right">
        Name: <input type="text">
        Passwort: <input type="text">
        <input type="submit" value="Anmelden">
        <a href="Registrierungsmaske.html">Registrierung</a>
    </form>
</div>



<div class="sidebar" >
    
  <a class="active" href="#home">Startseite</a>
  <a href="">
    </a>

    <ul class="nav row p-3 my-4 text-center" style="background-color:lightgrey">
        <li class="col-1 p-0 text-black h5" id="dropdownEpochen" data-bs-toggle="dropdown" aria-expanded="false">
            <a href="#">Schüler:innen</a>
        </li>
            <ul class="dropdown-menu col-1" aria-labelledby="dropdownEpochen">
                <li><a class="dropdown-item" href="#">Stundenplan</a></li>
                <li><a class="dropdown-item" href="#">Klasseneinteilung</a></li>
                <li><a class="dropdown-item" href="#">Noten</a></li>
            </ul>
    </ul>

    <ul class="nav row p-3 my-4 text-center" style="background-color:lightgrey">
        <li class="col-1 p-0 text-black h5" id="dropdownEpochen" data-bs-toggle="dropdown" aria-expanded="false">
            <a href="#">Schule</a>
        </li>
            <ul class="dropdown-menu col-1" aria-labelledby="dropdownEpochen">
                <li><a class="dropdown-item" href="Lehrerinnen.php">Klassenübersicht</a></li>
                <li><a class="dropdown-item" href="#">Stundenplan</a></li>
                <li><a class="dropdown-item" href="#">Noteneingabe</a></li>
            </ul>
    </ul>

    <ul class="nav row p-3 my-4 text-center" style="background-color:lightgrey">
        <li class="col-1 p-0 text-black h5" id="dropdownEpochen" data-bs-toggle="dropdown" aria-expanded="false">
            <a href="#">Land</a>
        </li>
            <ul class="dropdown-menu col-1" aria-labelledby="dropdownEpochen">
                <li><a class="dropdown-item" href="Entry.html">Relevante Seite1</a></li>
                <li><a class="dropdown-item" href="#">Notenübermittlung</a></li>
            </ul>
    </ul>

    <ul class="nav row p-3 my-4 text-center" style="background-color:lightgrey">
        <li class="col-1 p-0 text-black h5" id="dropdownEpochen" data-bs-toggle="dropdown" aria-expanded="false">
            <a href="#">Admin</a>
        </li>
            <ul class="dropdown-menu col-1" aria-labelledby="dropdownEpochen">
                <li><a class="dropdown-item" href="Entry.html">Epochenübersicht</a></li>
                <li><a class="dropdown-item" href="#">Epoche 1</a></li>
            </ul>
    </ul>
    </div>


<div class="content text-center">
    <br>
    <br>
    <section class="vh-100" style="background-color: #eee;">
    <div class="container h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-lg-12 col-xl-11">
          <div class="card text-black" style="border-radius: 25px;">
            <div class="card-body p-md-5">
              <div class="row justify-content-center">
                <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
  
                  <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Registrierung</p>
  
                  <form class="mx-1 mx-md-4">
  
                    <div class="d-flex flex-row align-items-center mb-4">
                      <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                      <div class="form-outline flex-fill mb-0">
                        <input type="text" id="UserName" class="form-control" />
                        <label class="form-label" for="UserName">User*innenname</label>
                      </div>
                    </div>


                    <div class="d-flex flex-row align-items-center mb-4">
                      <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                      <div class="form-outline flex-fill mb-0">
                        <input type="email" id="form3Example3c" class="form-control" />
                        <label class="form-label" for="form3Example3c">E-Mail</label>
                      </div>
                    </div>
  
                      
                    <div class="d-flex flex-row align-items-center mb-1">
                        <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                        <div class="form-outline flex-fill mb-0">
                          <input type="text" id="Vorname" class="form-control" />
                          <label class="form-label" for="Vorname">Vorname</label>
                        </div>
                      </div>


                      <div class="d-flex flex-row align-items-center mb-4">
                        <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                        <div class="form-outline flex-fill mb-0">
                          <input type="text" id="Nachname" class="form-control" />
                          <label class="form-label" for="Nachname">Nachname</label>
                        </div>
                      </div>


                    <div class="d-flex flex-row align-items-center mb-1">
                      <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                      <div class="form-outline flex-fill mb-0">
                        <input type="password" id="form3Example4c" class="form-control" />
                        <label class="form-label" for="form3Example4c">Passwort</label>
                      </div>
                    </div>
  
                    <div class="d-flex flex-row align-items-center mb-4">
                      <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                      <div class="form-outline flex-fill mb-0">
                        <input type="password" id="form3Example4cd" class="form-control" />
                        <label class="form-label" for="form3Example4cd">Passwort wiederholen</label>
                      </div>
                    </div>
  
                    <div class="form-check d-flex justify-content-center mb-5">
                      <input class="form-check-input me-2" type="checkbox" value="" id="form2Example3c" />
                      <label class="form-check-label" for="form2Example3">
                        Meine Zustimmung zu den <a href="#!">AGB</a>
                      </label>
                    </div>
  
                    <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                      <button type="button" class="btn btn-primary btn-lg">Registrierung abschließen</button>
                    </div>
  
                  </form>
  
                </div>                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div>
    <br>
    <h1>impressum</h1>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

<footer>
<h1>impressum</h1>
</footer>
</html>