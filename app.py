from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)


@app.route('/')
def index():
        return render_template('auth.html')
@app.route('/', methods = ['POST', 'GET'])
def index_post():
        return redirect(url_for('mainpage'))


@app.route('/mainpage', methods = ['POST', 'GET'])
def mainpage():
        return render_template('mainpage.html')

@app.route('/lehrende')
def lehrende():
        return render_template('lehrer.html')
@app.route('/schueler')
def schueler():
        return render_template('schueler.html')
@app.route('/admins')
def admins():
        return render_template('admins.html')
@app.route('/land')
def land():
        return render_template('land.html')
@app.route('/auth')
def auth():
        return render_template('auth.html')
@app.route('/auth', methods = ['POST'])
def auth_post():
        return render_template('schueler.html')


@app.route('/signup')
def signup():
        return render_template('signup.html')
@app.route('/ASteuerung')
def asteuerung():
        return render_template('ASteuerung.html')
@app.route('/ASteuerung', methods = ['POST'])
def asteuerung_post():
        return redirect(url_for('asteuerung'))

@app.route('/BNotenuebermittlung')
def bnotenuebermittlung():
        return render_template('BNotenuebermittlung.html')
@app.route('/Schueleruebersicht')
def schueleruebersicht():
        return render_template('BSchueleruebersicht.html')
@app.route('/LKlassenuebersicht')
def lklassenuebersicht():
        return render_template('LKlassenuebersicht.html')
@app.route('/LNoteneingabe')
def lnoteneingabe():
        return render_template('LNoteneingabe.html')
@app.route('/LStundenplan')
def lstundenplan():
        return render_template('LStundenplan.html')
@app.route('/SchNoten')
def schnoten():
        return render_template('SchNoten.html')
@app.route('/SchSchulanmeldung')
def schschulanmeldung():
        return render_template('SchSchulanmeldung.html')
@app.route('/SchStundenplan')
def schstundenplan():
        return render_template('SchStundenplan.html')
@app.route('/SchTermine')
def schtermine():
        return render_template('SchTermine.html')